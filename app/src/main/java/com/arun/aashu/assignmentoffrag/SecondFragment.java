package com.arun.aashu.assignmentoffrag;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class SecondFragment extends Fragment {

    View vie;
    TextView textView;
    String name1,phone1,gender;


    public SecondFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

       vie= inflater.inflate(R.layout.fragment_second, container, false);

       textView=vie.findViewById(R.id.text1);

          Bundle b = getArguments();

            name1=b.getString("Name_key");
        String phone1=b.getString("phone_key");
        String gender=b.getString("radio_key");
        String spin=b.getString("spin_key");

        textView.setText("Name: "+name1+"\n"+"Phone No: "+phone1+"\n"+"Gender: "+gender+"\n"+"Course Selected: "+spin);



        return vie;
    }

}
