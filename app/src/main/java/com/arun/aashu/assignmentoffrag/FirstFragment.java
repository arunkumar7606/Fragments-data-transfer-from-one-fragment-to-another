package com.arun.aashu.assignmentoffrag;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;


public class FirstFragment extends Fragment {

    View vi;
    RadioButton r1, r2;
    RadioGroup radioGroup;
    String name, phone, spiner, r = "Male";
    EditText ed1, ed2;
    Spinner sp;
    Button btn;

    String data[] = {"Select Course", "java", "Android", "C++", "C", "Python"};

    public FirstFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        vi = inflater.inflate(R.layout.fragment_first, container, false);

        ed1 = vi.findViewById(R.id.Nametext);
        ed2 = vi.findViewById(R.id.phone);
        sp = vi.findViewById(R.id.spin);
        radioGroup = vi.findViewById(R.id.rdgroup);
        r1 = vi.findViewById(R.id.radio1);
        r2 = vi.findViewById(R.id.radio2);
        btn = vi.findViewById(R.id.submitbtn);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {


                if (r1.isChecked()) {
                    r = r1.getText().toString();
                } else if (r2.isChecked()) {
                    r = r2.getText().toString();
                } else {
                    r = r2.getText().toString();
                }

            }
        });

//        String data[] = getResources().getStringArray(R.array.data);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, data);
        sp.setAdapter(adapter);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name = ed1.getText().toString();
                phone = ed2.getText().toString();
                spiner = sp.getSelectedItem().toString();

                if (name.length() >= 1 && phone.length() >= 1) {




                        SecondFragment fra = new SecondFragment();
                        Bundle bundle = new Bundle();

                        bundle.putString("Name_key", name);
                        bundle.putString("phone_key", phone);
                        bundle.putString("radio_key", r);
                        bundle.putString("spin_key", spiner);


                        fra.setArguments(bundle);

                        FragmentManager fm = getFragmentManager();
                        FragmentTransaction ft = fm.beginTransaction();

                        ft.replace(R.id.main_page, fra);
                        ft.addToBackStack(null);
                        ft.commit();


                } else {
                    ed1.setError("Fill Name");
                    ed2.setError("Fill Phone Number");

                }


            }
        });

        return vi;
    }

}
